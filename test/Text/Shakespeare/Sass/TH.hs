module Text.Shakespeare.Sass.TH where

import Text.Shakespeare.Sass (wsass')

import Data.Text.Lazy.Builder (toLazyText)
import Language.Haskell.TH
import Text.Internal.CssCommon (renderCssUrl)
import Yesod.Core.Widget (unCssBuilder)

renderSpliced :: Bool -> FilePath -> Q Exp
renderSpliced devel input = [|renderCssUrl undefined $(wsass' devel [] input)|]

renderUnspliced :: Bool -> FilePath -> Q Exp
renderUnspliced devel input = [|toLazyText $ unCssBuilder $ $(wsass' devel [] input)|]

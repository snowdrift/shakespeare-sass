import Text.Shakespeare.Sass
import Text.Shakespeare.Sass.TH

import Data.Text.Lazy (Text)
import Language.Haskell.TH
import Test.Hspec

main :: IO ()
main = hspec $ do
  describe "shakespeare-sass" $ do
    it "doesn't strip whitespace from spliced Sass in development mode" $
        let red = "#FF0000" :: Text in
            $(renderSpliced True "test/test.silius") `shouldBe`
            "body {\n    margin: 0;\n    color: #FF0000;\n}\n"
    it "strips whitespace from spliced Sass in production mode" $
        let red = "#FF0000" :: Text in
            $(renderSpliced False "test/test.silius") `shouldBe`
            "body{margin:0;color:#FF0000}"
    it "doesn't strip whitespace from unspliced Sass in development mode" $
        $(renderUnspliced True "test/test.sass") `shouldBe`
        "/* line 1, test/test.sass */\nbody {\n  margin: 0;\n  color: #AA0000; }\n"
    it "strips whitespace from unspliced Sass in production mode" $
        $(renderUnspliced False "test/test.sass") `shouldBe`
        "body{margin:0;color:#AA0000}\n"
